import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import {  Route, Routes} from 'react-router-dom';
import AdminHome from "./pages/admin/AdminHome";
import PageNotFound from "./pages/PageNotFound";
import Login from "./pages/admin/Login";
import AboutUs from './pages/client/AboutUs'
import NavBar from "./components/client/NavBar";
import HomeIntroduction from "./components/client/HomeIntroduction";
import EventCards from "./components/client/EventCards";
import Footer from "./components/client/Footer";
import ContactUs from "./pages/client/ContactUs";
import Gallery from "./pages/client/Gallery";
import AdminBookings from "./pages/admin/AdminBookings";

function App() {
  return (
    <div>
        <Routes>
          <Route path="/" element={
            <>
              <NavBar/>
              <HomeIntroduction/>
              <EventCards/>
              <Gallery />
              <Footer/>
            </>
          }/>
          <Route path="/about" element={
          <>
            <NavBar />
            <AboutUs />
            <Footer/>
          </>}/>
          <Route path="/contact" element={
          <>
            <NavBar/>
            <ContactUs />
            <Footer/>
          </>} />
          <Route path="/admin" element={<AdminHome />} />
          <Route path="/bookings" element={<AdminBookings/>} />
          <Route path="/login" element={<Login />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
    </div>
  );
}

export default App;
