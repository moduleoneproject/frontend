import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";
import axios from "axios";
import Header from "../../components/admin/Header";
import Bookings from "../../components/admin/Bookings";
import { BASE_URL } from "../../assets/helper";

const AdminBookings = () => {
  const navigate = useNavigate();
  const [changed, setChanged] = useState(false);
  const [cookies, removeCookie] = useCookies([]);
  const [username, setUsername] = useState("");
  useEffect(() => {
    const verifyCookie = async () => {
      if (!cookies.token) {
        navigate("/login");
      }
      const { data } = await axios.post(
        `${BASE_URL}`,
        {},
        { withCredentials: true }
      );
      const { status, user } = data;
      setUsername(user);
      return status
        ? console.log('Admin Log in')
        : (removeCookie("token"), navigate("/login"));
    };
    verifyCookie();
  }, [cookies, navigate, removeCookie]);
  const Logout = () => {
    removeCookie("token");
    console.log(cookies.token);
    navigate("/login");
  };
  return (
    <>
      <div>
        <Header changed={changed} setChanged={setChanged} Logout={Logout} username={username}/>
        <Bookings/>
      </div>
    </>
  );
};

export default AdminBookings;