import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import { Button } from "reactstrap";
import { BASE_URL } from "../../assets/helper";

const Login = () => {
  const navigate = useNavigate();
  const [inputValue, setInputValue] = useState({
    email: "",
    password: "",
  });
  const { email, password } = inputValue;
  const handleOnChange = (e) => {
    const { name, value } = e.target;
    setInputValue({
      ...inputValue,
      [name]: value,
    });
  };

  const handleError = (err) =>
    toast.error(err, {
      position: "bottom-left",
    });
  const handleSuccess = (msg) =>
    toast.success(msg, {
      position: "bottom-left",
    });

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const mainData = await axios.post(
        `${BASE_URL}login`,
        {
          ...inputValue,
        },
        { withCredentials: true }
      );
      const {data} = mainData;
      const { success, message } = data;
      if (success) {
        handleSuccess(message);
        setTimeout(() => {
          navigate("/admin");
        }, 1000);
      } else {
        handleError(message);
      }
    } catch (error) {
      console.error(error);
    }
    setInputValue({
      ...inputValue,
      email: "",
      password: "",
    });
  };

  return (
    <div
      style={{ height: "100vh" }}
      className="d-flex flex-column align-items-center justify-content-center gap-3 bg-image"
    >
      <Button
        onClick={() => navigate("/")}
        style={{ position: "absolute", top: "1rem", right: "1rem" }}
        color="primary"
      >
        Home
      </Button>
      <form
        style={{ opacity: "0.95" }}
        onSubmit={handleSubmit}
        className="d-flex flex-column gap-4 p-5 rounded-2 shadow bg-light"
      >
        <h3 className="text-center text-primary h1">Admin Login</h3>
        <div>
          <input
            className="form-control"
            type="email"
            name="email"
            value={email}
            placeholder="Email"
            onChange={handleOnChange}
          />
        </div>
        <div>
          <input
            className="form-control"
            type="password"
            name="password"
            value={password}
            placeholder="Password"
            onChange={handleOnChange}
          />
        </div>
        <button type="submit" className="btn btn-outline-primary">
          Submit
        </button>
      </form>
      <ToastContainer />
    </div>
  );
};

export default Login;
