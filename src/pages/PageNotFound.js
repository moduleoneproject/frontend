import React from 'react'

const PageNotFound = () => {
  return (
    <div className='d-flex align-items-center justify-content-center' style={{height:"100vh"}}>
      <h1 className='text-danger'>404 : Page not found</h1>
    </div>
  )
}

export default PageNotFound
