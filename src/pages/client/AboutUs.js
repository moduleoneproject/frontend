import React from "react";
function About() {
  return (
    <section className="about-us py-5 mt-5">
      <div className="container">
        <h1 className="mb-4 text-primary fw-medium">Our Story</h1>
        <p>
          At Trekking Trails, we believe that the best way to connect with the world is through immersive and awe-inspiring adventures. Our journey began with a group of avid trekkers who shared a common dream of creating a platform that celebrates the beauty of nature while fostering a sense of community among like-minded individuals.
        </p>

        <h1 className="mb-4 text-primary fw-medium">What Sets Us Apart</h1>
        <div className="row">
          <div className="col-md-4 mb-4">
            <img src="https://images.squarespace-cdn.com/content/v1/5b4544e485ede17941bc95fc/df7001a3-a0a3-4a17-a967-4f666a56416f/four-pass-loop-colorado-hikers-view.jpg" alt="Passionate Guides" className="rounded about-img" />
            <h3 className="mt-3">Passionate Guides</h3>
            <p>Our experienced and knowledgeable guides are not just experts in trekking but also enthusiasts who are deeply committed to sharing their love for the outdoors.</p>
          </div>
          <div className="col-md-4 mb-4">
            <img src="https://images.squarespace-cdn.com/content/v1/54335bc7e4b0eae9287f6186/1524460754609-UGT0KO96FCV91C8HWJXF/Hiking+the+Skyline+Trail+%7C+Beyond+Ordinary+Guides-35.jpg?format=1000w" alt="Curated Trails" className="rounded about-img" />
            <h3 className="mt-3">Curated Trails</h3>
            <p>We meticulously curate trekking trails that showcase the diverse landscapes and hidden gems of each destination. From breathtaking mountain vistas to serene forest paths, every trail tells a unique story.</p>
          </div>
          <div className="col-md-4 mb-4">
            <img src="https://png.pngtree.com/png-vector/20230408/ourmid/pngtree-safety-first-logo-design-vector-png-image_6685059.png" alt="Safety First" className="rounded about-img" />
            <h3 className="mt-3">Safety First</h3>
            <p>Your safety is our top priority. We adhere to the highest safety standards, ensuring that every trek is not only thrilling but also secure.</p>
          </div>
          {/* Repeat the structure for other highlights */}
        </div>

        <h1 className="mb-4 text-primary fw-medium">Our Commitment</h1>
        <p>
          Trekking Trails is committed to providing you with an unforgettable trekking experience that goes beyond the ordinary. Whether you are a seasoned trekker or a first-time explorer, we welcome you to join us on a journey of discovery, self-discovery, and camaraderie.
        </p>

        <p>Embark on a trek with us and let Trekking Trails be your guide to the wonders of the world. Adventure awaits!</p>
      </div>
    </section>
  );
}

export default About;
