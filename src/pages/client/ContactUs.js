import React from "react";
import { ImLocation } from "react-icons/im";
import { ImPhone } from "react-icons/im";
import { ImMail4 } from "react-icons/im";
const ContactUs = () => {
    return (
      <div style={{margin:"7rem 0 4rem 0"}}
        className="d-flex flex-column flex-lg-row justify-content-evenly"
      >
        <div className="d-flex align-items-center justify-content-center">
          <div>
            <h1 className="text-primary text-center">Contact Us</h1>
            <p className="text-center">
              <ImLocation /> Dharmraj Chowk, Akurdi, Pune, Maharashtra{" "}
            </p>
            <p className="text-center">
              {" "}
              <ImPhone /> +91 99999 99999
            </p>
            <p className="text-center">
              <ImMail4 /> hello@trekking.com
            </p>
          </div>
        </div>
          <form
            className="mx-sm-5 mx-md-5 mt-5 d-flex flex-column align-items-center justify-content-center gap-3"
            action="https://api.web3forms.com/submit"
            method="POST"
          >
              <h1 className="text-primary">Drop Your Message</h1>
              <input
                type="hidden"
                name="access_key"
                value="2ef2ac7b-4140-44de-bb27-7d9833605019"
              />
              <input className="form-control" type="text" name="name" placeholder="Name" required/>
            
              <input
                className="form-control"
                type="Number"
                name="Number"
                placeholder="Mobile Number"
                required
              />
              <input
                className="form-control"
                type="email"
                name="email"
                placeholder="Email"
                required
              />
              <textarea
                className="form-control"
                name="message"
                placeholder="write message"
                required
              ></textarea>
            <button type="submit" className="btn btn-outline-primary">
              Submit
            </button>
          </form>
        </div>
    );
  }

export default ContactUs;
