import React from "react";
import HomeIntroduction from "../../components/client/HomeIntroduction";
import Footer from "../../components/client/Footer";
import EventCards from "../../components/client/EventCards";
import { Route, Routes } from "react-router-dom";
import Events from "./Events";

const UserHome = () => {
  return (
    <div>
      
      <Routes>
        <Route  path="home"  element={<>
              <HomeIntroduction />
              <EventCards />
            </>
          }
        />
        <Route path='event' element={<Events/>} />
      </Routes>
      <Footer />
    </div>
  );
};

export default UserHome;
