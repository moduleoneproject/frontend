import React, { Component } from "react";

export default class Gallery extends Component {
  render() {
    return (
      <div className="my-5">
        <h1 style={{fontWeight:"bold"}} className="text-center my-5 text-primary">
          {" "}
          Gallery
        </h1>
        <div className="d-flex flex-wrap align-items-center justify-content-center gap-3">
          <img
            src="https://fh-sites.imgix.net/sites/3119/2019/11/26171333/Trekking-Group-at-Poonhill.jpg?auto=compress%2Cformat&w=1024&h=1024&fit=max"
            alt="img"
            id="gallery"
          />
          <img
            src="https://midurgveda.com/Admin/gallery-images/11.jpg"
            alt="img"
            id="gallery"
          />
          <img
            src="https://images.unsplash.com/photo-1533240332313-0db49b459ad6?w=600&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8dHJla2tpbmd8ZW58MHx8MHx8fDA%3D"
            alt="img"
            id="gallery"
          />
          <img
            src="https://images.unsplash.com/photo-1623390003556-3ac0b6602dba?w=600&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTh8fHRyZWtraW5nfGVufDB8fDB8fHww"
            alt="img"
            id="gallery"
          />
          <img
            src="https://images.unsplash.com/photo-1566371486037-6072a54daf1f?w=600&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTl8fHRyZWtraW5nfGVufDB8fDB8fHww"
            alt="img"
            id="gallery"
          />
          <img
            src="https://images.unsplash.com/photo-1496996317594-95a0cf707af5?w=600&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTV8fHRyZWtraW5nfGVufDB8fDB8fHww"
            alt="img"
            id="gallery"
          />
          
        </div>
      </div>
    );
  }
}
