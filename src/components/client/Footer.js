import React from 'react'
import {FaInstagram, FaYoutube, FaFacebook} from 'react-icons/fa'

const Footer = () => {
  return (
    <div className='footer'>
        <p style={{ fontSize: "1rem"}}>All copyrights reserved to Trekking Trails @2024</p>
        <div className='d-flex gap-3'>
            <FaInstagram className='icon'/>
            <FaYoutube className='icon'/>
            <FaFacebook className='icon'/>
        </div>
    </div>
  )
}

export default Footer
