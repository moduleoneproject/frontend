import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
} from "reactstrap";
import {Link} from 'react-router-dom'

function NavBar() {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar
      fixed="top"
        color="light"
        className="h5"
        expand="md"
        
      >
        <NavbarBrand href="/">
          <p style={{ fontFamily: "'Handlee', cursive"}} className="nav-bar-brand px-lg-5 pt-3">Trekking Trails</p>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="me-auto d-flex justify-content-end w-100 ms-3 mt-2" navbar>
            <NavItem>
              <Link onClick={toggle} to='/' style={{textDecoration:"none"}}><p  className="mx-3 nav-items mt-lg-2 fade-in-text">Home</p></Link>
            </NavItem>
            <NavItem>
              <Link onClick={toggle} to="/contact" style={{textDecoration:"none"}}><p  className="mx-3 nav-items mt-lg-2 fade-in-text">Contact Us</p></Link>
            </NavItem>
            <NavItem>
              <Link onClick={toggle} to="/about" style={{textDecoration:"none"}}><p  className="mx-3 nav-items mt-lg-2 fade-in-text">About Us</p></Link>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;
