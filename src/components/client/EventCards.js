import React, { useEffect, useState } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { fetchData } from "../../redux/action";
import { Card, CardBody, Button } from "reactstrap";
import Booking from "./Booking";
import { BASE_URL } from "../../assets/helper";
const getUrl = `${BASE_URL}getTrekEventData`;

const EventCards = () => {
  const [trekName, setTrekName] = useState("");
  const [trekId, setTrekId] = useState(0);
  const [modal, setModal] = useState(false);
  const dispatch = useDispatch();
  const data = useSelector((state) => state.data);
  useEffect(() => {
    axios
      .get(getUrl)
      .then((res) => dispatch(fetchData(res.data)))
      .catch((err) => console.error(err));
  }, [dispatch]);
  return (
    <>
      <h1
        style={{ fontWeight: "bold" }}
        className="text-center my-5 text-primary"
      >
        Trending Treks
      </h1>
      <div className="d-flex align-items-center justify-content-center gap-3 mb-3 flex-wrap">
        {data.map((event, index) => (
          <Card
            key={index}
            className="my-3 shadow card"
            style={{
              width: "19rem",
              borderRadius: "1rem",
            }}
          >
            <img
              style={{
                borderRadius: "1rem",
              }}
              className="card-img"
              alt="TrekImage"
              src={event.trekEventImg}
            />
            <CardBody className="d-flex flex-column align-items-center justify-content-center">
              <p style={{ fontWeight: "bold" }} className="h5">
                {event.trekEventName}
              </p>
              <p className="h5 my-2">{event.trekEventDate}</p>
              <span className="my-3">
                <span className="text-success">{`₹${event.trekEventPrice}/-`}</span>{" "}
                per person
              </span>
              <Button
                onClick={() => {
                  setModal(!modal);
                  setTrekName(event.trekEventName);
                  setTrekId(event.trekEventId);
                }}
                color="primary"
                outline
              >
                Book Now
              </Button>
            </CardBody>
          </Card>
        ))}
      </div>
      <Booking
        modal={modal}
        setModal={setModal}
        trekName={trekName}
        trekId={trekId}
      />
    </>
  );
};

export default EventCards;
