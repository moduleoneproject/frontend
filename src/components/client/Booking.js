import axios from "axios";
import React, { useRef } from "react";
import { Button, Modal } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import {BASE_URL} from '../../assets/helper'

function Booking({ modal, setModal, trekName, trekId }) {
  const toggle = () => setModal(!modal);
  const nameRef = useRef();
  const mailRef = useRef();
  const numberRef = useRef();
  const handleError = (err) => {
    toast.error(err, {
      position: "bottom-left",
    });
    return true;
  };
  const handleClick = () => {
    const userData = {
      trekEventId: trekId,
      name: nameRef.current.value,
      email: mailRef.current.value,
      contactNumber: parseInt(numberRef.current.value),
    };
    let number = userData.contactNumber.toString();
    if(!(number.length === 10)){
      handleError("Phone number length should be exactly 10");
      return;
    }
    if (!userData.name || !userData.email || !userData.contactNumber) {
      handleError("All fields are required");
      return;
    }
    toggle();
    axios
      .post(`${BASE_URL}insertBookingData`, userData)
      .then((res) => {
        console.log(res.data);
        toast.success(
          "Shortly we will connect through mail. Thank You for booking.",
          {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 10000,
          }
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      <ToastContainer />
      <Modal isOpen={modal} toggle={toggle} centered>
        <h2 className="text-center mt-3">Book Your Trek</h2>
        <form
          onSubmit={(event) => event.preventDefault()}
          className="d-flex flex-column gap-3 px-3"
        >
          <p className="text-center h5">Trek Name: {trekName}</p>
          <input
            className="form-control"
            type="text"
            name="name"
            id="name"
            placeholder="Name"
            ref={nameRef}
            required
          />
          <input
            className="form-control"
            type="email"
            name="email"
            id="email"
            placeholder="Email"
            ref={mailRef}
            required
          />
          <input
            className="form-control"
            type="number"
            name="phone"
            id="phone"
            placeholder="Phone Number"
            ref={numberRef}
            required
          />
          <div className="d-flex justify-content-center mb-2 gap-3">
            <Button
              color="primary"
              onClick={() => {
                handleClick();
              }}
              outline
              type="submit"
            >
              Book
            </Button>
            <Button color="dark" onClick={toggle} outline>
              Cancel
            </Button>
          </div>
        </form>
      </Modal>
    </div>
  );
}

export default Booking;
