import React from 'react'
import img from '../../assets/home-image.jpg'

const HomeIntroduction = () => {
  return (
    <div className='home-intro'>
      <p className='intro-sentence'>Welcome to Trekking Trails!</p>
      <img className='img' src={img} alt='home'/>
    </div>
  )
}

export default HomeIntroduction
