import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
} from "reactstrap";
import AddTrekForm from "./AddTrekForm";
import { useNavigate, Link } from "react-router-dom";

function Header({ changed, setChanged, Logout, username }) {
  const [modal, setModal] = useState(false);
  const navigate = useNavigate();
  const modalToggle = () => setModal(!modal);
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar
        color="light"
        className="h4 fixed-top"
        expand="md"
        
      >
        <NavbarBrand href="/">
          <p
            style={{ fontFamily: "'Handlee', cursive" }}
            className="nav-bar-brand px-lg-5 pt-3"
          >
            Trekking Trails
          </p>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav
            className="me-auto d-flex justify-content-end w-100 ms-3 mt-2"
            navbar
          >
            <NavItem>
              <Link to='/admin' style={{textDecoration:"none"}}><p  className="mx-3 nav-items mt-lg-2 fade-in-text">Events</p></Link>
            </NavItem>
            <NavItem>
              <Link to='/bookings' style={{textDecoration:"none"}}><p  className="mx-3 nav-items mt-lg-2 fade-in-text">Bookings</p></Link>
            </NavItem>
            <NavItem>
              <NavLink>
                <Button
                  onClick={() => setModal(!modal)}
                  color="primary"
                  outline
                >
                  Add Trek
                </Button>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink>
              <Button onClick={()=>navigate('/')} color="primary" outline>
                Home
              </Button>
              </NavLink>
            </NavItem>

            <NavItem>
              <NavLink>
                <Button onClick={Logout} color="primary" outline>
                  Log Out
                </Button>
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
      <AddTrekForm
        modal={modal}
        setModal={modalToggle}
        changed={changed}
        setChanged={setChanged}
      />
    </div>
  );
}

export default Header;
