import React, { useEffect, useState } from "react";
import axios from "axios";
import { MdDelete, MdEdit } from "react-icons/md";
import EditTrek from "./EditTrek";
import { BASE_URL } from "../../assets/helper";
const getUrl = `${BASE_URL}getTrekEventData`
const deleteUrl = `${BASE_URL}deleteTrekEvent`;

const Main = ({changed, setChanged}) => {
  const [modal, setModal] = useState(false);
  const modalToggle = () => setModal(!modal);
  const [edit, setEdit] = useState({
    flag : false,
    id : ''
  });
  const [data, setData] = useState([]);
  useEffect(() => {
    axios
      .get(getUrl)
      .then((res) => {
        setData(res.data);
      })
      .catch((err) => console.error(err));
  }, [changed]);
  const handleDelete = (id) => {
    setChanged(!changed)
    axios
      .delete(`${deleteUrl}/${id}`)
      .then((res) => console.log(res))
      .catch((err) => console.error(err));
  };
  return (
    <div style={{background: 'linear-gradient(45deg, #0d6efd, white)'}}  className="admin-main mt-5">
      <div className="col-sm-12 col-md-10 col-lg-8">
        <div style={{fontWeight:"bold"}} className="text-center display-6 text-dark">Trek Events</div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Trek Id</th>
              <th>Name</th>
              <th>Date</th>
              <th>Price</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {data.sort((a, b)=>a.trekEventId - b.trekEventId).map((eventData, index) => (
              <tr key={index}>
                <td>{eventData.trekEventId}</td>
                <td>{eventData.trekEventName}</td>
                <td>{eventData.trekEventDate}</td>
                <td>{eventData.trekEventPrice}</td>
                <td className="h5">
                  {<MdEdit onClick={()=>{setEdit({
                    ...edit,
                    flag: true,
                    id : eventData._id
                  })
                  modalToggle();
                  }} style={{ cursor: "pointer" }} />}
                </td>
                <td className="h5">
                  {
                    <MdDelete
                      onClick={() => handleDelete(eventData._id)}
                      style={{ cursor: "pointer" }}
                    />
                  }
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {  
      modal && <EditTrek modal={modal} setModal={setModal} edit={edit} changed={changed} setChanged={setChanged} />
      }
    </div>
  );
};

export default Main;
