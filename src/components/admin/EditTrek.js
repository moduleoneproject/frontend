import React, {useRef, useState} from 'react'
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { BASE_URL } from '../../assets/helper';
const updateUrl = `${BASE_URL}updateTrekEvent`;
const getUrl = `${BASE_URL}getTrekEventData`

const EditTrek = ({modal, setModal, edit, changed, setChanged}) => {
    const [data, setData] = useState([])
    const idRef = useRef();
    const nameRef = useRef();
    const imgRef = useRef();
    const dateRef = useRef();
    const priceRef = useRef();

    const handleEdit = () => {
        setChanged(!changed)
        const trekData = {
            trekEventId: idRef.current.value ? parseInt(idRef.current.value) : parseInt(idRef.current.defaultValue),
            trekEventName: nameRef.current.value ? nameRef.current.value : nameRef.current.defaultValue,
            trekEventImg: imgRef.current.value ? imgRef.current.value : imgRef.current.placeholder,
            trekEventDate : dateRef.current.value ? dateRef.current.value : dateRef.current.defaultValue,
            trekEventPrice: priceRef.current.value ? parseInt(priceRef.current.value) : parseInt(priceRef.current.defaultValue)
          }
        axios.put(`${updateUrl}/${edit.id}`, trekData)
        .then(res=>console.log(res))
        .catch(err=>console.error(err))
    }
    
    useState(()=>{
        axios.get(`${getUrl}/${edit.id}`)
        .then(res=>setData(res.data))
        .catch(err=>console.error(err))
    }, [])
  return (
    <div>
      <Modal isOpen={modal} toggle={() => setModal(!modal)} >
        <ModalHeader className="d-flex justify-content-center">Edit Trek Event</ModalHeader>
        <ModalBody className="d-flex flex-column gap-3">
          <input className='form-control' type="number" name="" id="id" defaultValue={data.map(item=>item.trekEventId)} ref={idRef} required/>
          <input className='form-control' type="text" name="" id="name" defaultValue={data.map(item=>item.trekEventName)} ref={nameRef} required/>
          <input className='form-control' type="text" name="" id="img" placeholder={data.map(item=>item.trekEventImg)} ref={imgRef} required/>
          <input className='form-control' type="date" name="" id="date" defaultValue={data.map(item=>item.trekEventDate)} ref={dateRef} required/>
          <input className='form-control' type="number" name="" id="price" defaultValue={data.map(item=>item.trekEventPrice)} ref={priceRef} required/>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-center">
          <Button color="primary" onClick={() =>{ 
            setModal(!modal)
            handleEdit()
            }}>
            Edit
          </Button>{' '}
          <Button color="secondary" onClick={() => setModal(!modal)}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  )
}

export default EditTrek
