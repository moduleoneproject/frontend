import axios from 'axios';
import React, { useRef, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {fetchData} from '../../redux/action'
import { ToastContainer, toast } from 'react-toastify';
import { BASE_URL } from '../../assets/helper';
const insertUrl = `${BASE_URL}insertTrekEvent`;
const getUrl = `${BASE_URL}getTrekEventData`;

function AddTrekForm({modal, setModal, changed, setChanged}) {
  const idRef = useRef();
  const nameRef = useRef();
  const imgRef = useRef();
  const dateRef = useRef();
  const priceRef = useRef();
  const data = useSelector(state=>state.data);
  const dispatch = useDispatch();
  useEffect(() => {
    axios
      .get(getUrl)
      .then((res) => dispatch(fetchData(res.data)))
      .catch((err) => console.error(err));
  }, [dispatch]);
  const handleClick = () => {

    const trekData = {
      trekEventId: parseInt(idRef.current.value),
      trekEventName: nameRef.current.value,
      trekEventImg: imgRef.current.value,
      trekEventDate: dateRef.current.value,
      trekEventPrice: parseInt(priceRef.current.value)
    }
    const {trekEventId, trekEventName, trekEventImg, trekEventDate, trekEventPrice} = trekData;
    if(!trekEventId || !trekEventName || !trekEventImg || !trekEventDate || !trekEventPrice){
      toast.error("All fields are required",{
        position: toast.POSITION.BOTTOM_LEFT
      });
      return;
    }
    if(data.some(item=>item.trekEventId===trekEventId)){
      toast.error("This id exist",{
        position: toast.POSITION.BOTTOM_LEFT
      });
      return;
    }
    setModal(!modal)
    setChanged(!changed)
    axios.post(insertUrl, trekData)
    .then(res=>console.log(res.data))
    .catch(err=>console.error(err))
  }

  return (
    <div>
      
      <Modal isOpen={modal} toggle={() => setModal(!modal)} >
        <ModalHeader className="d-flex justify-content-center">Add Trek Event</ModalHeader>
        <ModalBody className="d-flex flex-column gap-3">
          <input className='form-control' type="number" name="" id="id" placeholder='Id' ref={idRef} required/>
          <input className='form-control' type="text" name="" id="name" placeholder='Name' ref={nameRef} required/>
          <input className='form-control' type="text" name="" id="img" placeholder='Image Link' ref={imgRef} required/>
          <input className='form-control' type="date" name="" id="date" placeholder='Date' ref={dateRef} required/>
          <input className='form-control' type="number" name="" id="price" placeholder='Price' ref={priceRef} required/>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-center">
          <Button color="primary" onClick={() =>{ 
            handleClick()
            }}>
            Add
          </Button>{' '}
          <Button color="secondary" onClick={() => setModal(!modal)}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
      <ToastContainer/>
    </div>
  );
}

export default AddTrekForm;
