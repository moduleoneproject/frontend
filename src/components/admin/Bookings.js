import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { BASE_URL } from '../../assets/helper';

const Bookings = () => {
  const [data, setData] = useState([]);
  useEffect(()=>{
    axios.get(`${BASE_URL}getBookings`)
    .then(res=>setData(res.data))
    .catch(err=>console.error(err))
  }, [])
  return (
    <div style={{ height:"100vh",background: 'linear-gradient(45deg, #0d6efd, white)'}} className='d-flex flex-column align-items-center justify-content-center'>
      <h1 style={{fontWeight:"bold"}} className="text-center display-6 text-dark">User Bookings</h1>
      <div className='col-sm-12 col-md-10 col-lg-8'>
      <table className='table table-bordered table-striped '>
        <thead>
          <tr>
            <th>Trek Id</th>
            <th>Name</th>
            <th>Mail</th>
            <th>Contact</th>
          </tr>
        </thead>
        <tbody>
          {
            data.map((booking, index)=>(
              <tr key={index}>
                <td>{booking.trekEventId}</td>
                <td>{booking.name}</td>
                <td>{booking.email}</td>
                <td>{booking.contactNumber}</td>
              </tr>
            ))
          }
        </tbody>
      </table>
      </div>
    </div>
  )
}

export default Bookings
